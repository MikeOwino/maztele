<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $orgShortName = 'Maziwa Tele';
        $orgFullName = 'Maziwa Tele Limited';
        $this->app->singleton('orgShortName', function () use ($orgShortName) {
            return $orgShortName;
        });
        $this->app->singleton('orgFullName', function () use ($orgFullName) {
            return $orgFullName;
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}