<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class DatabaseHelper
{
    public static  function SaveReport($data)
    {
        $table = 'reports';
        DB::table($table)->insert($data);
    }

    public static function getUserByEmail($email)
    {
        return  DB::table('users')->where('email', $email)->first();
    }
}
