<?php

use Illuminate\Support\Facades\DB;


class TableFunctions
{


    public function getItem($id, $table)
    {
        $item = DB::table($table)->find('id', $id)->get();
        return $item;
    }

    public function getUnique($identifier, $table, $column)
    {
        $item = DB::table($table)->where($column, $identifier)->get();
        return $item;
    }
}
