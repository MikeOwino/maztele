<?php

namespace App\Console\Commands;

use App\OrganizationData;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class MinuteCommands extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:minute-commands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        logLivestock();
    }
}

function logLivestock()
{
    $livestock = DB::table('organizations')->where('deleted', false)->get();

    foreach ($livestock as $item) {
    }
}
