<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class RunMigrations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run-migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run php artisan migrate for all databases except the maziwa-tele-master';


    protected function configure()
    {
        $this->addOption('rollback', null, InputOption::VALUE_NONE, 'Roll back database migrations');
        $this->addOption('refresh', null, InputOption::VALUE_NONE, 'Roll back database migrations and migrate again');
    }


    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // ALL AVAILABLE DATABASES

        $databases = [
            'MAZIWA_TELE',
        ];

        $this->info('Starting Database Migrations');

        foreach ($databases as $database) {
            $migrationOption = 'migrate';
            if ($this->option('rollback')) {
                $migrationOption = 'migrate:rollback';
            } else if ($this->option('refresh')) {
                $migrationOption = 'migrate:refresh';
            }

            Artisan::call($migrationOption, [
                '--force' => true,
                '--database' => $database,
            ]);
            $this->info('Migrated for ' . $database);
        }




        $this->info('Migrations completed successfully!');
    }
}
