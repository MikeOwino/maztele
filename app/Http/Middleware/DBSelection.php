<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class DBSelection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle($request, Closure $next)
    {
        $databaseName = 'DEFAULT';
        $key = env('APP_KEY');
        $string = $request->header('organization');

        if ($string !== $databaseName) {
            $decryptedString = Crypt::decryptString($string, $key);
            $databaseName = json_decode($decryptedString);
        }

        config(['database.connections.mysql.database' => env($databaseName)]);
        return $next($request);
    }
}