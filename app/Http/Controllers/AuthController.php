<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\Auth;
use App\Models\auth;
use App\Models\User;
use App\Models\permissions;
use App\Models\organization;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\Rules\Password as RulesPassword;



class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    }

    public function register(Request $request)
    {
        $email = $request['email'];
        $username = $request['username'];
        $phone = $request['phone'];

        $getEmail = User::where('email', $email)->first();

        if ($getEmail) {
            $response = [
                'message' => 'error',
                'description' => "email $email is already taken",
            ];
            return response($response, 200);
        }

        $getUsername = User::where('username', $username)->first();

        if ($getUsername) {
            $response = [
                'message' => 'error',
                'description' => "Username $username is already taken",
            ];

            return response($response, 200);
        }

        $getPhone = User::where('phone', $phone)->first();

        if ($getPhone) {
            $response = [
                'message' => 'error',
                'description' => "Phone Number $phone is already taken",
            ];

            return response($response, 200);
        }

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'username' => $request['username'],
            'phone' => $request['phone'],
            'photoURL' => $request['photoURL'],
            'password' => bcrypt($request['password']),

            // "1|hbgV4cxTUPQuzWIS2EDUcBLQJFt9YzlrwaQp5icj"
        ]);
        $token = $user->createToken('maziwa-tele')->plainTextToken;
        $user['permissions'] = permissions::where('userId', $user['id']);
        $this->resetPassword($request);
        $response = [
            'message' => 'success',
            'user' => $user,
            'token' => $token,
        ];

        return response($response, 201);
    }

    public function login(Request $request)
    {


        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required',
        ]);


        // check email
        $user = User::where('email', $fields['email'])->first();

        //password
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response(['message' => 'Wrong email or password'], 401);
        } else {
            $token = $user->createToken('maziwa-tele')->plainTextToken;
            $user['permissions'] = permissions::where('userId', $user['id'])->get();

            $response = [
                'user' => $user,
                'token' => $token,
            ];
            return response($response, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // $perm = permissions::where('userId', $id);

    }

    public function getPermission($id)
    {
    }





    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        // $user = User::find($id);
        $user = "permi>get()";
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(auth $auth)
    {
        //
    }

    /**
     * check whether a user's laravel token is valid
     */
    public function checkTokenValidity()
    {
        // return response('valid', 200);
        // DB::table('personal_access_tokens')->where()
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, auth $auth)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(auth $auth)
    {
        //
    }

    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();
        return response('Logged Out', 201);
    }
}