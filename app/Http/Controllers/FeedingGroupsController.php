<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFeedingGroupsRequest;
use App\Http\Requests\UpdateFeedingGroupsRequest;
use App\Models\FeedingGroups;
use Illuminate\Http\Request;

class FeedingGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return FeedingGroups::where('deleted', false)->get();
    }

    public function getAll()
    {
        return FeedingGroups::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $name = $request['name'];

        $groupNameExists = FeedingGroups::where('name', $name)->first();

        if ($groupNameExists) {
            $response = [
                'data' => 'Error',
                'message' => 'A feeding group named ' . $name . ' already exists',
            ];
            $statusCode = 200;
        } else {
            $input = $request->all();
            $feed = FeedingGroups::create($input);

            $response = [
                'data' => $feed,
                'message' => 'success',
            ];
            $statusCode = 201;
        }
        return response($response, $statusCode);
    }

    /**
     * Display the specified resource.
     */
    public function show(FeedingGroups $feedingGroups)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FeedingGroups $feedingGroups)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFeedingGroupsRequest $request, FeedingGroups $feedingGroups)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $group = FeedingGroups::find($id);
        return $group->update([
            'deleted' => true,
        ]);
    }

    public function restore($id)
    {
        $group = FeedingGroups::find($id);
        return $group->update([
            'deleted' => false,
        ]);
    }
}
