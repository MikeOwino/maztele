<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFeedBatchesRequest;
use App\Http\Requests\UpdateFeedBatchesRequest;
use App\Models\FeedBatches;
use Illuminate\Http\Request;

class FeedBatchesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return FeedBatches::where('deleted', false)->where('quantity', '>', 0)->where('status', '!=', 'expired')->get();
    }

    public function getAll()
    {
        return FeedBatches::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $name = $request['name'];
        $batch = FeedBatches::where('name', $name)->where('deleted', false)->where('status', 'active')->first();
        if (!$batch) {
            $request->merge(['status' => 'active']);
        }
        $input = $request->all();
        $feedBatch = FeedBatches::create($input);

        $response = [
            'data' => $feedBatch,
            'message' => 'success',
        ];
        $statusCode = 201;
        return response($response, $statusCode);
    }

    /**
     * Display the specified resource.
     */
    public function show(FeedBatches $feedBatches)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FeedBatches $feedBatches)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id, Request $request)
    {
        $batch = FeedBatches::find($id);
        $batch::update($request->all());


        $response = [
            'data' => $batch,
            'message' => 'success',
        ];
        $statusCode = 201;
        return response($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $group = FeedBatches::find($id);
        return $group->update([
            'deleted' => true,
        ]);
    }

    public function restore($id)
    {
        $group = FeedBatches::find($id);
        return $group->update([
            'deleted' => false,
        ]);
    }
}