<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoremilkingGroupsRequest;
use App\Http\Requests\UpdatemilkingGroupsRequest;
use App\Models\milkingGroups;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MilkingGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return milkingGroups::where('deleted', false)->get();
    }

    public function getAll()
    {
        return milkingGroups::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // milkingGroups::create([$request->all()])
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $name = $request['name'];

        $groupNameExists = milkingGroups::where('name', $name)->first();

        if ($groupNameExists) {
            $response = [
                'data' => 'Error',
                'message' => 'Group name ' . $name . ' already exists',
            ];
            $statusCode = 200;
        } else {
            $input = $request->all();
            $milkingGroup = MilkingGroups::create($input);

            $response = [
                'data' => $milkingGroup,
                'message' => 'success',
            ];
            $statusCode = 201;
        }
        return response($response, $statusCode);
    }

    /**
     * Display the specified resource.
     */
    public function show(milkingGroups $milkingGroups)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(milkingGroups $milkingGroups)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatemilkingGroupsRequest $request, milkingGroups $milkingGroups)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $group = milkingGroups::find($id);
        return $group->update([
            'deleted' => true,
        ]);
    }

    public function restore($id)
    {
        $group = milkingGroups::find($id);
        return $group->update([
            'deleted' => false,
        ]);
    }
}
