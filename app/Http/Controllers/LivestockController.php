<?php

namespace App\Http\Controllers;
// use App\H
use App\Models\livestock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ReportsController;

class LivestockController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return livestock::where('deleted', false)->get();
    }

    public function getAll()
    {
        return livestock::all();
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // ReportsController::SaveReport()
        $user = get_object_vars(ReportsController::getUserByEmail($request['registeredBy']));
        $name = $user['name'];
        $phone = $user['phone'];
        $tagId = $request['tagId'];
        $livestock = livestock::create($request->all());

        $report = [
            'type' => 'livestock',
            'title' => 'Livestock Added',
            'message' => "$name ,$phone, added livestock tag id $tagId",
            'info' => 'livestock',
        ];
        ReportsController::SaveReport($report);
        return $livestock;
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        return livestock::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Livestock $livestock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $livestock = livestock::find($id);
        return $livestock->update($request->all());
    }

    public function getLivestockBytagId(Request $request)
    {

        $tagId = $request['tagId'];
        return livestock::where('tagId', $tagId)->first()->get();

        // $id = $request['id'];
        // if ($id) {
        //     $lS = livestock::where('tagId', $tagId)->where('id', '!=', $id)->first();
        //     if ($lS) {
        //         return true;
        //     } else {
        //         return null;
        //     }
        // } else {
        //     return livestock::where('tagId', $tagId)->first()->get();
        // }
    }



    public function uploadImage(Request $request)
    {
        $path = 'public/images/' . $request['path'];
        if (!$request['path']) {
            $path = 'public/images';
        }

        if ($request->hasFile('image')) {
            // $image = $request->file('image');
            // $imageName = time() . '.' . $image->getClientOriginalExtension();
            // $image->move(public_path('uploads'), $imageName);
            // $imageUrl = '/uploads/images/livestock' . $imageName;
            // return ['imageUrl' => $imageUrl];
            $image = $request->file('image');
            $filename = $image->store($path);
            $url = Storage::url($filename);
            return response()->json(['url' => asset($url)]);
        }
    }

    public function deleteImage($id)
    {
        $livestock = $this->show($id);


        $url = $livestock['photoURL'];

        if ($url) {
            // get the path of the file from the URL
            $path = parse_url($url, PHP_URL_PATH);

            // delete the file using the Storage facade
            $delete = Storage::disk('public')->delete($path);
            // $this
        }

        $livestock = livestock::find($id);
        return $livestock->update([
            'photoURL' => null,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $feed = livestock::find($id);
        return $feed->update([
            'deleted' => true,
        ]);
    }

    public function restore($id)
    {
        $feed = livestock::find($id);
        return $feed->update([
            'deleted' => false,
        ]);
    }
}