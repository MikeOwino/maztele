<?php

namespace App\Http\Controllers;

use App\Models\owners;
use Illuminate\Http\Request;

class OwnersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(owners $owners)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(owners $owners)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, owners $owners)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(owners $owners)
    {
        //
    }
}
