<?php

namespace App\Http\Controllers;

use App\Models\permissions;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return permissions::where('deleted', false)->get();
    }

    public function getAll()
    {
        return permissions::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    // public function store(Request $request)
    // {
    //     return permissions::create($request->all());
    // }
    public function store(Request $request)
    {
        $permissionsArray = $request->all();
        $permissions = $permissionsArray['permissions'];
        foreach ($permissions as $permissions) {
            $permission = $permissions;
            $item = new permissions();
            $item->userId = $permission['userId'];
            $item->permission = $permission['permission'];
            $item->admin = $permission['admin'];
            $item->create = $permission['create'];
            $item->read = $permission['read'];
            $item->update = $permission['update'];
            $item->delete = $permission['delete'];
            $item->professional = $permission['professional'];
            $item->save();
        }
        return response([
            'status' => 'Milking hours added Successfully.',
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        return permissions::where('userId', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(permissions $permissions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $permissions = permissions::find($id);
        return $permissions->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $feed = permissions::find($id);
        return $feed->update([
            'deleted' => true,
        ]);
    }

    public function restore($id)
    {
        $feed = permissions::find($id);
        return $feed->update([
            'deleted' => false,
        ]);
    }
}