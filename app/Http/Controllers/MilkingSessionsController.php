<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMilkingSessionsRequest;
use App\Http\Requests\UpdateMilkingSessionsRequest;
use App\Models\MilkingSessions;
use Illuminate\Http\Request;

class MilkingSessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return MilkingSessions::where('deleted', false)->get();
    }

    public function getAll()
    {
        return MilkingSessions::all();
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user = get_object_vars(ReportsController::getUserByEmail($request['milkedBy']));
        $name = $user['name'];
        $phone = $user['phone'];
        $litres = $request['litres'];
        $tagId = $request['livestockId'];
        $report = [
            'type' => 'livestock',
            'title' => 'Livestock Milked',
            'message' => "$name ,$phone, milked $litres litres from livestock tag id $tagId",
            'info' => 'milked',
        ];
        $query = MilkingSessions::create($request->all());

        ReportsController::SaveReport($report);
        return $query;
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        return MilkingSessions::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MilkingSessions $milkingSessions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $livestock = MilkingSessions::find($id);
        return $livestock->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $milkingSession = MilkingSessions::find($id);
        return $milkingSession->update([
            'deleted' => true,
        ]);
    }

    public function restore($id)
    {
        $milkingSession = MilkingSessions::find($id);
        return $milkingSession->update([
            'deleted' => false,
        ]);
    }
}