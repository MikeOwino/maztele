<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFeedingHoursRequest;
use App\Http\Requests\UpdateFeedingHoursRequest;
use App\Models\FeedingHours;
use Illuminate\Http\Request;

class FeedingHoursController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return FeedingHours::where('deleted', false)->get();
    }

    public function getAll()
    {
        return FeedingHours::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $feedingHours = FeedingHours::create($input);

        $response = [
            'data' => $feedingHours,
            'message' => 'success',
        ];
        $statusCode = 201;
        return response($response, $statusCode);
    }

    /**
     * Display the specified resource.
     */
    public function show(FeedingHours $feedingHours)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FeedingHours $feedingHours)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFeedingHoursRequest $request, FeedingHours $feedingHours)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FeedingHours $feedingHours)
    {
        //
    }
}