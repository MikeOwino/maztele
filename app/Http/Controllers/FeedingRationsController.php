<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFeedingRationsRequest;
use App\Http\Requests\UpdateFeedingRationsRequest;
use App\Models\FeedingRations;
use Illuminate\Http\Request;

class FeedingRationsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return FeedingRations::where('deleted', false)->get();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $rationsArray = $request->all();
        // $rations = $rationsArray['milkingrationsWithId'];
        foreach ($rationsArray as $ration) {
            $item = new FeedingRations();
            $item->feedingHourId = $ration['id'];
            $item->feed = $ration['name'];
            $item->quantity = $ration['quantity'];

            $item->save();
        }
        return response([
            'status' => 'Milking rations added Successfully.',
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(FeedingRations $feedingRations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FeedingRations $feedingRations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFeedingRationsRequest $request, FeedingRations $feedingRations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FeedingRations $feedingRations)
    {
        //
    }
}