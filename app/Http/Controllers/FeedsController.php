<?php

namespace App\Http\Controllers;

use App\Models\feeds;
use Illuminate\Http\Request;

class FeedsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return feeds::where('deleted', false)->get();
    }

    public function getAll()
    {
        return feeds::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $name = $request['name'];

        $groupNameExists = Feeds::where('name', $name)->first();

        if ($groupNameExists) {
            $response = [
                'data' => 'Error',
                'message' => 'A feed named ' . $name . ' already exists',
            ];
            $statusCode = 200;
        } else {
            $input = $request->all();
            $feed = Feeds::create($input);

            $response = [
                'data' => $feed,
                'message' => 'success',
            ];
            $statusCode = 201;
        }
        return response($response, $statusCode);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        return feeds::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(feeds $feeds, $id)
    {
        // $feed = feeds::find($id);
        // return $feed->update($feeds->all());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $feed = feeds::find($id);
        return $feed->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($name)
    {
        $feed = feeds::where('name', $name)->first();
        return $feed->update([
            'deleted' => true,
        ]);
    }

    public function restore($name)
    {
        $feed = feeds::where('name', $name)->first();
        return $feed->update([
            'deleted' => false,
        ]);
    }
}
