<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoremilkingHoursRequest;
use App\Http\Requests\UpdatemilkingHoursRequest;
use App\Models\milkingHours;
use Illuminate\Http\Request;

class MilkingHoursController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return milkingHours::where('deleted', false)->get();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $milkingHoursArray = $request->all();
        $milkingHours = $milkingHoursArray['milkingHoursWithId'];
        foreach ($milkingHours as $hourArray) {
            $hour = $hourArray;
            $item = new milkingHours();
            $item->groupId = $hour['groupId'];
            $item->startTime = $hour['startTime'];
            $item->stopTime = $hour['stopTime'];

            $item->save();
        }
        return response([
            'status' => 'Milking hours added Successfully.',
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(milkingHours $milkingHours)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(milkingHours $milkingHours)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatemilkingHoursRequest $request, milkingHours $milkingHours)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(milkingHours $milkingHours)
    {
        //
    }
}