<?php

namespace App\Http\Controllers;

use App\Models\organization;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // return organization::where('deleted', false)->get();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // select maziwa_tele_master as the db
        DB::connection('DEFAULT')->table('organizations')->insert($request->all());
        return DB::connection('DEFAULT')->table('organizations')->where('orgCode', $request['orgCode'])->first();
        // insert new organization details
    }

    public function addDays($id, $days)
    {
        $org = organization::find($id);
        $org->daysLeft += $days;
        $org->status = 'active';
        $org->save();
        // organization::
    }

    public function encrypt(Request $request)
    {
        $key = env('APP_KEY');
        $string = $request['value'];
        $encryptedString = Crypt::encryptString($string, $key);

        return json_encode($encryptedString);
        // return $encryptedString;
    }

    public function decrypt(Request $request)
    {
        $key = env('APP_KEY');
        $string = $request['value'];
        $decryptedString = Crypt::decryptString($string, $key);

        return json_encode($decryptedString);
        // return $encryptedString;
    }


    // public function setCookie()
    // {
    //     $response = new Response(['organization' => 'MAZIWA_TELE']);
    //     $response->withCookie(cookie('organization', 'MAZIWA_TELE', (60 * 10000), null, null, false, false)); // Set the HttpOnly flag to true
    //     return $response;
    // }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        return organization::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(organization $organization)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, organization $organization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(organization $organization)
    {
        //
    }
}