<?php

namespace App\Http\Controllers;

use App\Models\ownership;
use Illuminate\Http\Request;

class OwnershipController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ownership $ownership)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ownership $ownership)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ownership $ownership)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ownership $ownership)
    {
        //
    }
}
