<?php

namespace App\Http\Controllers;

use App\Models\ownerships;
use Illuminate\Http\Request;

class OwnershipsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ownerships $ownerships)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ownerships $ownerships)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ownerships $ownerships)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ownerships $ownerships)
    {
        //
    }
}
