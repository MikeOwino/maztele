<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FeedBatchesController;
use App\Http\Controllers\FeedingGroupsController;
use App\Http\Controllers\FeedingHoursController;
use App\Http\Controllers\FeedingRationsController;
use App\Http\Controllers\FeedsController;
use App\Http\Controllers\LivestockController;
use App\Http\Controllers\MilkingGroupsController;
use App\Http\Controllers\MilkingHoursController;
use App\Http\Controllers\MilkingSessionsController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::resource('organization', OrganizationController::class);
Route::post('login', [AuthController::class, 'login']);
Route::post('encrypt', [OrganizationController::class, 'encrypt']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    route::get('/verify-token', [AuthController::class, 'checkTokenValidity']);
    Route::get('/user',  function (Request $request) {
        return $request->user();
    });

    // users
    Route::post('/register', [AuthController::class, 'register']);
    Route::get('/users/find/:id', [AuthController::class, 'show']);
    Route::resource('users', UsersController::class);

    // Livestock
    Route::resource('livestock', LivestockController::class);
    Route::get('/livestock/all', [LivestockController::class, 'getAll']);
    Route::post('/livestock/check-tag', [LivestockController::class, 'getLivestockBytagId']);
    Route::post('/livestock/upload-image', [LivestockController::class, 'uploadImage']);
    Route::get('/livestock/delete-image/{id}', [LivestockController::class, 'deleteImage']);


    // Feeds
    Route::resource('feeds', FeedsController::class);
    Route::get('/feeds/all', [FeedsController::class, 'getAll']);


    // feeding groups
    Route::resource('feeding-groups', FeedingGroupsController::class);
    Route::get('/feeding-groups/all', [FeedingGroupsController::class, 'getAll']);
    Route::get('feeding-groups/restore/{id}', [FeedingGroupsController::class, 'restore']);

    //feeding hours
    Route::resource('feeding-hours', FeedingHoursController::class);
    Route::get('/feeding-hours/all', [FeedingHoursController::class, 'getAll']);

    //feeding rations
    Route::resource('feeding-rations', FeedingRationsController::class);

    //feed batches
    Route::resource('feed-batches', FeedBatchesController::class);
    Route::get('/feed-batches/all', [FeedBatchesController::class, 'getAll']);
    Route::get('feed-batches/restore/{id}', [FeedBatchesController::class, 'restore']);


    //milking-groups
    Route::resource('milking-groups', MilkingGroupsController::class);
    Route::get('/milking-groups/all', [MilkingGroupsController::class, 'getAll']);


    // milking hours
    Route::resource('milking-hours', MilkingHoursController::class);

    // milking sessions
    Route::resource('milking-sessions', MilkingSessionsController::class);
    Route::get('/milking-sessions/all', [MilkingSessionsController::class, 'getAll']);
    Route::get('milking-sessions/restore/{id}', [MilkingSessionsController::class, 'restore']);

    //permissions
    Route::resource('permissions', PermissionsController::class);
    Route::get('/permissions/all', [PermissionsController::class, 'getAll']);
    Route::get('permissions/restore/{id}', [PermissionsController::class, 'restore']);

    // Authentication
    Route::get('/logout', [AuthController::class, 'logout']);

    //organizations
    Route::resource('organizations', OrganizationController::class);
});


// TODO: php /path/to/your/laravel/project/artisan schedule:work >> /dev/null 2>&1