<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('feeding_hours', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('feedingGroupId');
            $table->string('startTime');
            $table->string('stopTime');
            $table->boolean('deleted')->default(false);
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('feeding_hours');
    }
};