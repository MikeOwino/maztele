<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->id();
            $table->string('orgCode')->unique();
            $table->string('shortName');
            $table->string('fullName');
            $table->string('phoneNumber');
            $table->string('email');
            $table->text('description')->nullable();
            $table->string('imageURL')->nullable();
            $table->string('daysLeft')->default(10);
            $table->boolean('deleted')->default(false);
            $table->string('status')->default('active');
            $table->string('dateFounded')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('organizations');
    }
};
