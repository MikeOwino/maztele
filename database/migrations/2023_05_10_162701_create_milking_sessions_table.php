<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('milking_sessions', function (Blueprint $table) {
            $table->id();
            $table->string('livestockId');
            $table->string('day');
            $table->string('litres');
            $table->string('timeOfDay');
            $table->string('milkedBy');
            $table->text('remarks')->nullable();
            $table->string('status')->default('active');
            $table->boolean('deleted')->default(false);



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('milking_sessions');
    }
};