<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('livestocks', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable(false);
            $table->string('gender')->nullable(false);
            $table->string('breed')->nullable(false);
            $table->string('weight')->nullable(false);
            $table->string('photoURL')->nullable();
            $table->string('tagId')->nullable(false)->unique();
            $table->string('motherTagId')->nullable(false);
            $table->string('fatherTagId')->nullable(false);
            $table->string('feedingGroup')->nullable(false);
            $table->string('milkingGroup')->nullable(false);
            $table->string('dateOfBirth');
            $table->string('registeredBy')->nullable(false);
            $table->string('status')->default('ALIVE');
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('livestocks');
    }
};